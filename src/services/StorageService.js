import Service from "./Service";

export default class StorageService implements Service {

    start() {

    }

    stop() {

    }

    getHistory(key) {
        let rawHistory = localStorage.getItem(key);
        let history;
        if (rawHistory === undefined || rawHistory === null) {
            history = [];
        }
        else {
            try {
                history = JSON.parse(rawHistory);
            } catch (e) {
                console.log("malformed history");
                history = [];
            }
        }
        return history;
    }

    saveHistory(key, values) {
        localStorage.setItem(key, JSON.stringify(values));
    }

    insertValueAtHistoryBeginning(key, value) {
        let history = this.getHistory(key);
        history.unshift(value);
        this.saveHistory(key, history);
    }

    insertValueAtHistoryEnding(key, value) {
        let history = this.getHistory(key);
        history.push(value);
        this.saveHistory(key, history);
    }

    clearHistory(key)
    {
        localStorage.removeItem(key);
    }
}