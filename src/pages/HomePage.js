import React from "react";
import {Grid} from "react-bootstrap";

export default class HomePage extends React.Component {

    render() {
        return (
            <div>
                <Grid>
                    <h2>Travaillons ensemble !
                    </h2>
                    <p>J'espère que cette application réalisée rapidement saura convaincre...<br/>
                        J'aurais clairement pu améliorer des points :
                        <ul>
                            <li>Faire en sorte que la musique continue de jouer lorsque l'on change d'onglet
                            </li>
                            <li>Faire une intégration plus travaillée</li>
                            <li>Des tests unitaires...</li>
                            <li>Certainement des bonnes pratiques à travailler sur la partie HTML/CSS</li>
                        </ul>
                        Quoi qu'il arrive cela aura été une bonne expérience pour découvrir React en trois
                        jours.<br/>
                        J'apprécie beaucoup l'accent qui a été mis sur la réusabilité des composants par rapport
                        à Angular.<br/>
                        Bonne journée !<br/>
                        Harold
                    </p>
                </Grid>
            </div>
        );
    }

}