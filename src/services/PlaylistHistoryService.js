import ServiceRepository from "./ServiceRepository";
import Service from "./Service";

export default class PlaylistHistoryService implements Service {
    PLAYLIST_HISTORY_KEY = "playlist_history_key";

    start() {
    }

    stop() {

    }

    musicAddedAtBeginning(libraryElement) {
        ServiceRepository.getInstance().storageService.insertValueAtHistoryBeginning(this.PLAYLIST_HISTORY_KEY, libraryElement);
    }

    musicAddedAtEnding(libraryElement) {
        ServiceRepository.getInstance().storageService.insertValueAtHistoryEnding(this.PLAYLIST_HISTORY_KEY, libraryElement);
    }

    musicRemovedFromPlaylist(libraryElement) {
        let history = ServiceRepository.getInstance().storageService.getHistory(this.PLAYLIST_HISTORY_KEY);

        const valueIndex = history.map(x => x.filename).indexOf(libraryElement.filename);
        if (valueIndex !== -1) {
            history.splice(valueIndex, 1);
            ServiceRepository.getInstance().storageService.saveHistory(this.PLAYLIST_HISTORY_KEY, history);
        }
    }

    clearHistory() {
        ServiceRepository.getInstance().storageService.clearHistory(this.PLAYLIST_HISTORY_KEY);
    }

    getHistory() {
        return ServiceRepository.getInstance().storageService.getHistory(this.PLAYLIST_HISTORY_KEY);
    }
}
