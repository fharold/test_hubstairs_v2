export default class Utils {
    static containsOnlyDigits(value) {
        return /^\d+$/.test(value);
    }

    static readTextFile(onComplete, path) {
        fetch(path)
            .then(response => response.text())
            .then(text => onComplete(text));

        // let rawFile = new XMLHttpRequest();
        // rawFile.open("GET", path, false);
        // rawFile.onreadystatechange = function () {
        //     if (rawFile.readyState === 4) {
        //         if (rawFile.status === 200 || rawFile.status === 0) {
        //             onComplete(rawFile.responseText);
        //         }
        //     }
        // };
        // rawFile.send(null);
    }
}