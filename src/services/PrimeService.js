import PrimeResult from "../models/PrimeResult";
import ServiceRepository from "./ServiceRepository";
import Service from "./Service";

export default class PrimeService implements Service {
    MAX_NUMBER_INT_64 = 9223372036854775807;

    start() {

    }

    stop() {

    }

    /**
     * @param primeNumberPosition
     * @returns {PrimeResult}
     */
    computePrime(primeNumberPosition) {
        let primesFound = 0;
        primeNumberPosition = parseInt(primeNumberPosition, 10);
        if (primeNumberPosition >= 1) {
            if (primeNumberPosition >= 10000) {
                primeNumberPosition = 10000;
            }
            for (let i = 1; i < this.MAX_NUMBER_INT_64 && primeNumberPosition > primesFound; i++) {
                if (this.isPrime(i)) {
                    primesFound++;
                    if (primesFound === primeNumberPosition) {
                        const result = new PrimeResult(primeNumberPosition, i);
                        this.saveResult(result);
                        return result;
                    }
                }
            }
        }
        return undefined;
    }

    /**
     * @param primeResult
     */
    saveResult(primeResult) {
        ServiceRepository.getInstance().primeHistoryService.archivePrimeResult(primeResult);
    }

    /**
     * @param num
     * @returns {boolean}
     */
    isPrime(num) {
        if (num < 2) return false;
        for (let i = 2; i < num; i++) {
            if (num % i === 0)
                return false;
        }
        return true;
    }
}