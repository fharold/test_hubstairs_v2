import React from "react";
import PrimeComputer from "../components/PrimeComputer";
import PrimeHistoryViewer from "../components/PrimeHistoryViewer";

export default class PrimePage extends React.Component {

    render() {
        return (
            <div>
                <PrimeComputer/>
                <PrimeHistoryViewer/>
            </div>
        );
    }

}