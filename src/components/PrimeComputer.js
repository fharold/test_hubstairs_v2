import React from "react";
import {Button, Col, ControlLabel, FormControl, FormGroup, Grid, HelpBlock, Row, Well} from "react-bootstrap";
import ServiceRepository from "../services/ServiceRepository";
import Utils from "../misc/Utils";

export default class PrimeComputer extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.handleChange = this.handleChange.bind(this);
        this.computeClick = this.computeClick.bind(this);
        this.state = {
            value: '',
            computeReady: false,
            showResults: false,
            result: -1
        };
    }

    getValidationState() {
        if (this.state.value.length === 0 || Utils.containsOnlyDigits(this.state.value)) {
            return 'success';
        }
        return 'error';
    }


    refreshComputeButton() {
        this.setState();
    }

    handleChange(event) {
        const value = event.target.value;
        this.setState({
            value: value,
            computeReady: value.length !== 0 && Utils.containsOnlyDigits(value)
        });
        this.refreshComputeButton();
    }

    computeClick() {
        const result = ServiceRepository.getInstance().primeService.computePrime(this.state.value);
        this.setState({result: result.primeNumber, showResults: true});
    }

    render() {
        return (
            <div>
                <Grid>
                    <h2>Calcul du n-ième nombre premier</h2>
                    <form>
                        <FormGroup controlId="inputPrimeNumber"
                                   validationState={this.getValidationState()}>
                            <ControlLabel>Attention, une saisie au delà de
                                5000 peut nécessiter plusieurs dizaines de secondes.</ControlLabel>
                            <Row>
                                <Col xs={9} md={10}>
                                    <FormControl
                                        type="text"
                                        value={this.state.value}
                                        placeholder="Saisissez un entier positif"
                                        onChange={this.handleChange}
                                    />
                                </Col>
                                <Col xs={3} md={2}>
                                    <Button bsStyle="primary" disabled={!this.state.computeReady} block
                                            onClick={this.computeClick}>Calcul</Button>
                                </Col>
                            </Row>
                            <HelpBlock>Toute saisie au delà de 10000 sera bridée au moment du calcul afin d'éviter un
                                gel de
                                l'application.</HelpBlock>
                        </FormGroup>
                    </form>
                    {this.state.showResults && <Well>Résultat : {this.state.result}</Well>}
                </Grid>
            </div>
        );
    }
}