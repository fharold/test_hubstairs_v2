import React from "react";
import {Button, Grid, Table} from "react-bootstrap";
import PrimeHistoryViewerItem from "./PrimeHistoryViewerItem";
import ServiceRepository from "../services/ServiceRepository";

export default class PrimeHistoryViewer extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = PrimeHistoryViewer.getInitialState();
        this.onHistoryChanged = this.onHistoryChanged.bind(this);
        this.handleClick = this.handleClick.bind(this);
    }

    handleClick() {
        ServiceRepository.getInstance().primeHistoryService.clearHistory();
        this.refreshStateWithLatestHistory();
    }

    componentDidMount() {
        this.refreshStateWithLatestHistory();
        ServiceRepository.getInstance().primeHistoryService.addListener(this.onHistoryChanged);
    }

    componentWillUnmount() {
        ServiceRepository.getInstance().primeHistoryService.removeListener(this.onHistoryChanged);
    }

    onHistoryChanged() {
        this.refreshStateWithLatestHistory();
    }

    static getInitialState() {
        return {
            history: []
        };
    }

    refreshStateWithLatestHistory() {
        this.setState(
            {history: ServiceRepository.getInstance().primeHistoryService.getPrimeResultHistory()}
        );
    }

    render() {
        return <div>
            <Grid>
                <h2>Historique des calculs</h2>
                <Table striped bordered condensed hover responsive>
                    <thead>
                    <tr>
                        <th>n</th>
                        <th>n-ième nombre premier</th>
                    </tr>
                    </thead>
                    <tbody>
                    {
                        this.state.history.map(function (historyElement) {
                            return <PrimeHistoryViewerItem key={historyElement.timestamp} primeResult={historyElement}/>
                        })
                    }
                    </tbody>
                </Table>
                <Button bsStyle="danger" size="xsmall" block onClick={this.handleClick}>Supprimer l'historique</Button>
            </Grid>
        </div>
    }
}