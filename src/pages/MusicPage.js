import React from "react";
import MusicPlayer from "../components/MusicPlayer";
import PlaylistViewer from "../components/PlaylistViewer"
import LibraryViewer from "../components/LibraryViewer"
import {Col, Grid, Row} from "react-bootstrap";

export default class MusicPage extends React.Component {

    render() {
        return (
            <div>
                <Grid>
                    <Row>
                        <Col xs={12} md={12}>
                            <MusicPlayer/>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={12} md={6}>
                            <PlaylistViewer/>
                        </Col>
                        <Col xs={12} md={6}>
                            <LibraryViewer/>
                        </Col>
                    </Row>
                </Grid>
            </div>
        );
    }

}