import React from "react";
import ServiceRepository from "../services/ServiceRepository";
import MusicService from "../services/MusicService";

export default class MusicPlayer extends React.Component {
    audioPlayer;
    musicService;

    constructor(props, context) {
        super(props, context);
        this.musicChanged = this.musicChanged.bind(this);
        this.songHasFinished = this.songHasFinished.bind(this);
        this.musicService = ServiceRepository.getInstance().musicService;
    }

    componentDidMount() {
        this.musicService.addListener(this.musicChanged);
        this.audioPlayer = document.getElementById("audioPlayer");
    }

    loadMusic() {
        this.audioPlayer.addEventListener("ended", this.songHasFinished);

        if (this.musicService.currentMusic !== undefined) {
            this.changeAudioSource(this.musicService.currentMusic.filename);

            this.audioPlayer.load();
            this.audioPlayer.play();
        }
    }

    changeAudioSource(newSource) {
        let source = document.getElementById("audioSource");
        source.src = newSource;
    }

    songHasFinished() {
        this.musicService.currentMusicFinished();
    }

    musicChanged(status) {
        if (status === MusicService.MUSIC_NEXT || status === MusicService.MUSIC_PLAY) {
            this.loadMusic();
        } else if (status === MusicService.MUSIC_STOP) {
            this.audioPlayer.pause();
            this.changeAudioSource("");
            this.audioPlayer.load();
        }
    }

    render() {
        return (
            <div>
                <h2>Lecteur de musique</h2>
                <audio id="audioPlayer" controls>
                    <source id="audioSource" src=""/>
                </audio>
            </div>
        );
    }

}