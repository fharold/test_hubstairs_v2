import ServiceRepository from "./ServiceRepository";
import Service from "./Service"

export default class PrimeHistoryService implements Service {
    PRIME_HISTORY_KEY = "prime_history_key";
    listeners;

    start() {

    }

    stop() {

    }

    constructor() {
        this.listeners = [];
    }

    archivePrimeResult(primeResult) {
        ServiceRepository.getInstance().storageService.insertValueAtHistoryBeginning(this.PRIME_HISTORY_KEY, primeResult);
        this.notifyListeners();
    }

    getPrimeResultHistory() {
        return ServiceRepository.getInstance().storageService.getHistory(this.PRIME_HISTORY_KEY);
    }

    clearHistory() {
        ServiceRepository.getInstance().storageService.clearHistory(this.PRIME_HISTORY_KEY);
    }

    addListener(listener) {
        this.listeners.push(listener);
    }

    removeListener(listener) {
        const listenerIndex = this.listeners.indexOf(listener);
        if (listenerIndex !== -1) {
            this.listeners.splice(listenerIndex, 1);
        }
    }

    notifyListeners() {
        for (let i = 0; i < this.listeners.length; i++) {
            this.listeners[i]();
        }
    }
}
