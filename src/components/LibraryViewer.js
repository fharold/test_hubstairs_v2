import React from "react";
import ServiceRepository from "../services/ServiceRepository";
import LibraryItem from "./LibraryItem";
import Table from "react-bootstrap/es/Table";

export default class LibraryViewer extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.onLibraryUpdated = this.onLibraryUpdated.bind(this);
        this.state =
            {
                library: []
            }
    }

    componentDidMount() {
        ServiceRepository.getInstance().libraryService.addListener(this.onLibraryUpdated);
        this.fetchLibraryAndRefreshComponent();
    }

    componentWillUnmount() {
        ServiceRepository.getInstance().libraryService.removeListener(this.onLibraryUpdated());
    }

    fetchLibraryAndRefreshComponent() {
        this.setState({
            library: ServiceRepository.getInstance().libraryService.library
        });
    }

    onLibraryUpdated() {
        this.fetchLibraryAndRefreshComponent();
    }

    render() {
        return <div>
            <h2>Bibliothèque</h2>
            <Table striped bordered condensed hover responsive>
                <thead>
                <tr>
                    <th>Titre</th>
                    <th>Artiste</th>
                    <th/>
                    <th/>
                </tr>
                </thead>
                <tbody>
                {
                    this.state.library.map(function (libraryElement) {
                        return <LibraryItem key={libraryElement.id} libraryElement={libraryElement}/>
                    })
                }
                </tbody>
            </Table>
        </div>
    }
}