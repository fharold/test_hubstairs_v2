import LibraryElement from "../models/LibraryElement";
import Service from "./Service";

export default class LibraryService implements Service {
    listeners;
    MUSIC_CDN = "http://fritsch.pro/hubstairs/";

    start() {

    }

    stop() {

    }

    library = [
        new LibraryElement(0, this.MUSIC_CDN + "bc_ma.mp3", "Butterfly Caught", "Massive Attack"),
        new LibraryElement(1, this.MUSIC_CDN + "c_asl.mp3", "CAFO", "Animals as Leaders"),
        new LibraryElement(2, this.MUSIC_CDN + "c_b.mp3", "Broken", "Calibre"),
        new LibraryElement(3, this.MUSIC_CDN + "fp_r.mp3", "Realise", "Floating Points"),
        new LibraryElement(4, this.MUSIC_CDN + "wp_rs.mp3", "War/Peace", "Ryuichi Sakamoto"),
    ];

    constructor() {
        this.listeners = [];
    }

    addListener(listener) {
        this.listeners.push(listener);
    }

    removeListener(listener) {
        const listenerIndex = this.listeners.indexOf(listener);
        if (listenerIndex !== -1) {
            this.listeners.splice(listenerIndex, 1);
        }
    }

    notifyListeners() {
        for (let i = 0; i < this.listeners.length; i++) {
            this.listeners[i]();
        }
    }
}