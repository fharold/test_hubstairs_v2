import ServiceRepository from "./ServiceRepository";
import Service from "./Service";

export default class PlaylistService implements Service {
    listeners = [];
    playlist = [];
    playlistHistoryService;

    start() {
        this.playlistHistoryService = ServiceRepository.getInstance().playlistHistoryService;
        this.playlist = this.playlistHistoryService.getHistory();
        this.notifyListeners();
    }

    stop() {

    }

    getFirstSong() {
        if (this.playlist.length > 0) {
            return this.playlist[0];
        }
        return undefined;
    }

    removeDuplicateIfNecessary(libraryElement) {
        const indexOfElement = this.playlist.map(x => x.filename).indexOf(libraryElement.filename);
        if (indexOfElement !== -1) {
            this.playlist.splice(indexOfElement, 1);
            this.playlistHistoryService.musicRemovedFromPlaylist(libraryElement);
        }
    }

    insertMusicAtBeginning(libraryElement) {
        this.removeDuplicateIfNecessary(libraryElement);
        this.playlist.unshift(libraryElement);
        this.playlistHistoryService.musicAddedAtBeginning(libraryElement);
        this.notifyListeners();
    }

    insertMusicAtEnd(libraryElement) {
        this.removeDuplicateIfNecessary(libraryElement);
        this.playlist.push(libraryElement);
        this.playlistHistoryService.musicAddedAtEnding(libraryElement);
        this.notifyListeners();
    }

    removeMusic(libraryElement) {
        const libraryElementIndex = this.playlist.indexOf(libraryElement);
        if (libraryElementIndex === -1) {
            return;
        }
        this.playlistHistoryService.musicRemovedFromPlaylist(libraryElement);
        this.playlist.splice(libraryElementIndex, 1);
        this.notifyListeners();
    }

    addListener(listener) {
        this.listeners.push(listener);
    }

    removeListener(listener) {
        const listenerIndex = this.listeners.indexOf(listener);
        if (listenerIndex !== -1) {
            this.listeners.splice(listenerIndex, 1);
        }
    }

    notifyListeners() {
        for (let i = 0; i < this.listeners.length; i++) {
            this.listeners[i]();
        }
    }
}