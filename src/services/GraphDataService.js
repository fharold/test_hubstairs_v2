import Utils from "../misc/Utils";
import data_graph from "../assets/graph/data_graph.csv";
import Papa from "papaparse"
import Service from "./Service";

export default class GraphDataService implements Service {
    data;
    listeners = [];
    isDataReady = false;

    start() {

    }

    stop() {

    }

    constructor() {
        this.initializeData = this.initializeData.bind(this);
        this.initializeData();
    }

    addListener(listener) {
        this.listeners.push(listener);
    }

    removeListener(listener) {
        const listenerIndex = this.listeners.indexOf(listener);
        if (listenerIndex !== -1) {
            this.listeners.splice(listenerIndex, 1);
        }
    }

    notifyListeners() {
        for (let i = 0; i < this.listeners.length; i++) {
            this.listeners[i](this.isDataReady);
        }
    }

    formatData(data) {
        let formattedData = [];
        for (let i = 0; i < data.length; i++) {
            if (i === 0) {
                //if we are at the first row, get the header line in order to make a pretty array.
            }
            else {
                formattedData.push({
                    "date": String(data[i][0]),
                    "credit": Number(data[i][1]),
                    "debit": -Number(data[i][2]),
                    "balance": Number(data[i][3])
                });
            }
        }
        return this.sortData(formattedData);
    }

    sortData(data) {
        data.sort(function (a, b) {
            const aDate = a.date;
            const splittedADate = aDate.split('/');
            const bDate = b.date;
            const splittedBDate = bDate.split('/');

            if (splittedADate[2] !== splittedBDate[2]){
                return +splittedADate[2] - +splittedBDate[2];
            }
            if (splittedADate[0] !== splittedBDate[0]){
                return +splittedADate[0] - +splittedBDate[0];
            }
            return +splittedADate[1] - +splittedBDate[1];
        });
        return data;
    }

    initializeData() {
        Utils.readTextFile(rawData => {
            this.data = this.formatData(Papa.parse(rawData).data);
            this.isDataReady = true;
            this.notifyListeners();
        }, data_graph);
    }
}