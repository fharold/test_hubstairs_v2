import React from "react";
import {HashRouter} from "react-router-dom";
import Switch from "react-router-dom/es/Switch";
import {Navbar, Grid, Nav, NavItem} from "react-bootstrap";
import '../index.css';

export default class Header extends React.Component {

    render() {
        return (
            <header>
                <Switch>
                    <HashRouter>
                        <Grid fluid={true}>
                            <Navbar>
                                <Navbar.Header>
                                    <Navbar.Brand>
                                        <a href="/">Bienvenue sur mon test d'entrée chez Hubstairs :)</a>
                                    </Navbar.Brand>
                                    <Navbar.Toggle />
                                </Navbar.Header>
                                <Nav>
                                    <NavItem eventKey={1} href="/music">
                                        Lecteur de musique
                                    </NavItem>
                                    <NavItem eventKey={2} href="/graph">
                                        Graphe
                                    </NavItem>
                                    <NavItem eventKey={3} href="/prime">
                                        Calculateur du n-ième nombre premier
                                    </NavItem>
                                </Nav>
                            </Navbar>
                        </Grid>
                    </HashRouter>
                </Switch>
            </header>
        );
    }

}