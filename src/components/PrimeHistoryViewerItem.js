import React from "react";

export default class PrimeHistoryViewerItem extends React.Component {
    render() {
        return <tr>
            <td>{this.props.primeResult.primeNumberPosition}</td>
            <td>{this.props.primeResult.primeNumber}</td>
        </tr>
    }
}