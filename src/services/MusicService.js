import Service from "./Service";
import ServiceRepository from "./ServiceRepository";

export default class MusicService implements Service {
    listeners;
    currentMusic;
    playlistService;

    static MUSIC_STOP = "MUSIC_STOP";
    static MUSIC_PLAY = "MUSIC_PLAY";
    static MUSIC_NEXT = "MUSIC_NEXT";

    start() {
        this.listeners = [];
        this.playlistService = ServiceRepository.getInstance().playlistService;
    }

    stop() {

    }

    playMusic(libraryElement) {
        this.currentMusic = libraryElement;
        this.notifyListeners(MusicService.MUSIC_PLAY);
    }

    currentMusicFinished() {
        this.playlistService.removeMusic(this.currentMusic);

        this.currentMusic = this.playlistService.getFirstSong();

        if (this.currentMusic === undefined) {
            this.notifyListeners(MusicService.MUSIC_STOP);
        } else {
            this.notifyListeners(MusicService.MUSIC_NEXT);
        }
    }

    addListener(listener) {
        this.listeners.push(listener);
    }

    removeListener(listener) {
        const listenerIndex = this.listeners.indexOf(listener);
        if (listenerIndex !== -1) {
            this.listeners.splice(listenerIndex, 1);
        }
    }

    notifyListeners(status) {
        for (let i = 0; i < this.listeners.length; i++) {
            this.listeners[i](status);
        }
    }
}