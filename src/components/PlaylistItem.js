import React from "react";
import ServiceRepository from "../services/ServiceRepository";
import {Button} from "react-bootstrap";

export default class PlaylistItem extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.handlePlayClick = this.handlePlayClick.bind(this);
        this.handleRemoveClick = this.handleRemoveClick.bind(this);
    }

    handlePlayClick() {
        ServiceRepository.getInstance().musicService.playMusic(this.props.libraryElement);
        ServiceRepository.getInstance().playlistService.insertMusicAtBeginning(this.props.libraryElement);
    }

    handleRemoveClick() {
        ServiceRepository.getInstance().musicService.currentMusicFinished();
        ServiceRepository.getInstance().playlistService.removeMusic(this.props.libraryElement);
    }

    render() {
        return (
            <tr>
                <td>{this.props.libraryElement.title}</td>
                <td>{this.props.libraryElement.artist}</td>
                <td><Button bsStyle="primary" onClick={this.handlePlayClick}><i className="fas fa-play"/></Button></td>
                <td><Button bsStyle="primary" onClick={this.handleRemoveClick}><i className="fas fa-minus-circle"/></Button></td>
            </tr>
        );
    }

}