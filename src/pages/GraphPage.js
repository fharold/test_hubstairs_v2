import React from "react";
import GraphDisplayer from "../components/GraphDisplayer";
import {Grid} from "react-bootstrap";

export default class GraphPage extends React.Component {

    render() {
        return (
            <div>
                <Grid>
                <h1>Graphe</h1>
                <GraphDisplayer/>
                </Grid>
            </div>
        );
    }

}