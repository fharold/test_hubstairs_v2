import React from "react";
import {ComposedChart, Line, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend} from "recharts";
import ServiceRepository from "../services/ServiceRepository";

export default class GraphDisplayer extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            data: []
        };
        this.onDataReady = this.onDataReady.bind(this);
    }

    componentDidMount() {
        let graphDataService = ServiceRepository.getInstance().graphDataService;
        graphDataService.addListener(this.onDataReady);
        if (graphDataService.isDataReady) {
            this.setState({
                data: graphDataService.data
            })
        }
    }

    componentWillUnmount() {
        ServiceRepository.getInstance().graphDataService.removeListener(this.onDataReady());
    }

    onDataReady(dataReady) {
        if (dataReady) {
            this.setState({
                data: ServiceRepository.getInstance().graphDataService.data
            })
        }
    }

    render() {
        return (
            <div id="chart">
                <ComposedChart width={1200} height={700}
                               data={this.state.data}>
                    <CartesianGrid stroke='#f5f5f5'/>
                    <XAxis dataKey="date"/>
                    <YAxis/>
                    <Tooltip/>
                    <Legend/>
                    <Bar dataKey='debit' barSize={1000} fill='#FF0000'/>
                    <Bar dataKey='credit' barSize={1000} fill='#00FF00'/>
                    <Line type='natural' dataKey='balance' stroke='#0000FF'/>
                </ComposedChart>
            </div>
        );
    }
}