export default class PrimeResult {
    primeNumberPosition;
    primeNumber;
    timestamp;

    constructor(primeNumberPosition, primeNumber)
    {
        this.primeNumberPosition = primeNumberPosition;
        this.primeNumber = primeNumber;
        this.timestamp = new Date().getTime();
    }
}