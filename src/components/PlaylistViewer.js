import React from "react";
import PlaylistItem from "./PlaylistItem";
import {Table} from "react-bootstrap";
import ServiceRepository from "../services/ServiceRepository";

export default class PlaylistViewer extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.onPlaylistUpdated = this.onPlaylistUpdated.bind(this);
        this.state =
            {
                playlist: []
            }
    }

    componentDidMount() {
        ServiceRepository.getInstance().playlistService.addListener(this.onPlaylistUpdated);
        this.fetchPlaylistAndRefreshComponent();
    }

    componentWillUnmount() {
        ServiceRepository.getInstance().playlistService.removeListener(this.onPlaylistUpdated());
    }

    fetchPlaylistAndRefreshComponent() {
        this.setState({
            playlist: ServiceRepository.getInstance().playlistService.playlist
        });
    }

    onPlaylistUpdated() {
        this.fetchPlaylistAndRefreshComponent();
    }

    render() {
        return <div>
            <h2>Liste de lecture</h2>
            <Table striped bordered condensed hover responsive>
                <thead>
                <tr>
                    <th>Titre</th>
                    <th>Artiste</th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                {
                    this.state.playlist.map(function (playlistElement) {
                        return <PlaylistItem key={playlistElement.id} libraryElement={playlistElement}/>
                    })
                }
                </tbody>
            </Table>
        </div>
    }

}