import PrimeService from "./PrimeService";
import StorageService from "./StorageService";
import PrimeHistoryService from "./PrimeHistoryService";
import GraphDataService from "./GraphDataService";
import PlaylistService from "./PlaylistService";
import LibraryService from "./LibraryService";
import MusicService from "./MusicService";
import PlaylistHistoryService from "./PlaylistHistoryService";

export default class ServiceRepository {
    static instance = null;
    primeService;
    primeHistoryService;
    storageService;
    graphDataService;
    playlistService;
    playlistHistoryService;
    libraryService;
    musicService;

    constructor() {
        this.primeService = new PrimeService();
        this.storageService = new StorageService();
        this.primeHistoryService = new PrimeHistoryService();
        this.graphDataService = new GraphDataService();
        this.playlistService = new PlaylistService();
        this.libraryService = new LibraryService();
        this.musicService = new MusicService();
        this.playlistHistoryService = new PlaylistHistoryService();
    }

    startServices() {
        this.primeService.start();
        this.storageService.start();
        this.primeHistoryService.start();
        this.graphDataService.start();
        this.playlistService.start();
        this.libraryService.start();
        this.musicService.start();
        this.playlistHistoryService.start();
    }

    stopServices() {
        this.primeService.stop();
        this.storageService.stop();
        this.primeHistoryService.stop();
        this.graphDataService.stop();
        this.playlistService.stop();
        this.libraryService.stop();
        this.musicService.stop();
        this.playlistHistoryService.stop();
    }

    static getInstance() {
        if (ServiceRepository.instance == null) {
            ServiceRepository.instance = new ServiceRepository();
            ServiceRepository.instance.startServices();
        }
        return ServiceRepository.instance;
    }
}