import React from "react";
import {Button} from "react-bootstrap";
import ServiceRepository from "../services/ServiceRepository";

export default class LibraryItem extends React.Component {

    constructor(props, context) {
        super(props, context);
        this.handlePlayClick = this.handlePlayClick.bind(this);
        this.handleAddClick = this.handleAddClick.bind(this);
    }

    handlePlayClick() {
        ServiceRepository.getInstance().musicService.playMusic(this.props.libraryElement);
        ServiceRepository.getInstance().playlistService.insertMusicAtBeginning(this.props.libraryElement);
    }

    handleAddClick() {
        ServiceRepository.getInstance().playlistService.insertMusicAtEnd(this.props.libraryElement);
    }

    render() {
        return (
            <tr>
                <td>{this.props.libraryElement.title}</td>
                <td>{this.props.libraryElement.artist}</td>
                <td><Button bsStyle="primary" onClick={this.handlePlayClick}><i className="fas fa-play"/></Button></td>
                <td><Button bsStyle="primary" onClick={this.handleAddClick}><i className="fas fa-plus"/></Button></td>
            </tr>
        );
    }

}