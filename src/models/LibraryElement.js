export default class LibraryElement {
    id;
    filename;
    artist;
    title;

    constructor(id, filename, title, artist) {
        this.id = id;
        this.filename = filename;
        this.title = title;
        this.artist = artist;
    }
}