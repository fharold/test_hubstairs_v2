import React, {Component} from "react";
import Header from "./components/Header";
import GraphPage from "./pages/GraphPage";
import PrimePage from "./pages/PrimePage";
import MusicPage from "./pages/MusicPage";
import {Route} from "react-router-dom";
import HomePage from "./pages/HomePage";

class Main extends Component {

    render() {
        return (
            <div>
                <Header/>
                <div className="content">
                    <Route exact path="/" component={HomePage}/>
                    <Route path="/music" component={MusicPage}/>
                    <Route path="/graph" component={GraphPage}/>
                    <Route path="/prime" component={PrimePage}/>
                </div>
            </div>
        );
    }

}

export default Main;